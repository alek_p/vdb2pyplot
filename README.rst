Python script to help with graphing vdbench output.
Requires Python's **numpy** and **matplotlib** libraries to be present.

v0.6 Usage:
    ./vdb2pyplot.py -f flatfile.html <-h> <-a | -i | -b | -l | -c> <-n #_samples_per_run> <-p #_color_cycle_len>
        * -f path to vdbench flatfile.html (required)
        * -h print this help message
        * -a plot all graphs (-l -i -b -c) (default)
        * -i plot IOPS graph
        * -b plot MB/s graph
        * -l plot latency (ms) graph
        * -c plot cpu utilization (usr+sys) % graph
        * -n samples per run ( elapsed / interval )
        * -p restart the graph color pattern after this many colors
