#!/usr/bin/python
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

# Copyright 2015 Nexenta Systems, Inc.  All rights reserved.

import os, sys, getopt

from datetime import datetime
import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

vdb_path = "./vdb/vdbench"

version = "v0.7"

# display usage and exit
def usage():
	print version + " Usage:\n    %s -f flatfile.html <-h> <-a | -i | -b | -l | -c> <-n #_samples_per_run> <-p #_color_cycle_len>" % sys.argv[0]
	print("\t -f path to vdbench flatfile.html (required)")
	print("\t -h print this help message")
	print("\t -a plot all graphs (-l -i -b -c) (default)")
	print("\t -i plot IOPS graph")
	print("\t -b plot MB/s graph")
	print("\t -l plot latency (ms) graph")
	print("\t -c plot cpu utilization (usr+sys) % graph")
	print("\t -n samples per run ( elapsed / interval )")
	print("\t -p restart the graph color pattern after this many colors")
	sys.exit(1)

# return the pre compression blocksize, assuming it potentially ranges from 512b to 128kib and is power of two
def closest_xfersize(post_compression):
	possible = [512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576]
	for p in possible:
		if (p - post_compression >= -1): # compression could theoretically inflate block size
			if (p == 512):
				return '512b'
			elif (p == 1048576):
				return '1m'

			return str(p / 1024) + "k"
	return '?'

def get_samples_per_run(intervals):
	prev = 0
	for n in intervals:
		if (prev != 0 and n == 1):
			return int(prev)
		prev = n

	return int(n)

def get_sec_per_sample(tod):
	t1 = datetime.strptime(tod[0],'%H:%M:%S.%f')
	t2 = datetime.strptime(tod[1],'%H:%M:%S.%f')

	return int((t2-t1).total_seconds()) + 1

# save plot to cur dir
def generate_bar_plot(plot_type, run_names, data, samples_per_run, color_cycle_pattern, flat_file):
	plot = ['ERROR', 'IOPS', 'MB_p_s', "latency_in_ms", "CPU_utilization_prcnt"]
	y = []
	label = []
	std = []

	num_runs = len(run_names) / samples_per_run
	run_start = 1 # skip first datapoint (at 0)
	for run in range(1, 1 + num_runs):
		run_end = run * samples_per_run
		label.append(run_names[run_start] + " | xf " + closest_xfersize(numpy.average(data[:,5][run_start:run_end])) + " | " + `round(numpy.average(data[:,plot_type][run_start:run_end]), 2)`)
		#print "Processing run " + `run` + " of " + `len(run_names) / samples_per_run`
		y.append(numpy.average(data[:,plot_type][run_start:run_end]))
		std.append(numpy.std(data[:,plot_type][run_start:run_end]))
		run_start = run * samples_per_run + 1 # skip first data point

	x = numpy.arange(num_runs)
	plt.bar(x, y, color = [ c for c in colors[0:color_cycle_pattern] ], yerr = std)

	plt.ylabel(plot[plot_type])

	#major_ticks = numpy.arange(0, 20, 5)
	#minor_ticks = numpy.arange(0, 20, 1)
	ax = plt.axes()
	#ax.set_yticks(major_ticks)
	#ax.set_yticks(minor_ticks, minor=True)
	ax.yaxis.grid(True) # horizontal lines

	plt.xticks(x+0.5, label, rotation='vertical', fontsize=10);

	fig = plt.gcf()
	fig.set_size_inches(20, 10)
	fig.tight_layout()
	plt.autoscale(tight=True)
	x1,x2,y1,y2 = plt.axis()
	plt.axis((x1,x2,0,y2))
	plt.savefig(os.path.splitext(plot[plot_type] + "." + os.path.basename(flat_file))[0] + ".png")
	plt.clf()

	print "Generated " + plot[plot_type] + " plot - " + os.path.splitext(plot[plot_type] + "." + os.path.basename(flat_file))[0] + ".png"

# save plot to cur dir
def generate_line_plot(plot_type, run_names, data, sec_per_sample, samples_per_run, color_cycle_pattern, flat_file):
	plot = ['ERROR', 'IOPS', 'MB_p_s', "latency_in_ms", "CPU_utilization_prcnt"]
	label = []

	num_runs = len(run_names) / samples_per_run
	run_start = 1 # skip first datapoint (at 0)
	for run in range(1, 1 + num_runs):
		run_end = run * samples_per_run
		#print "Processing run " + `run` + " of " + `len(run_names) / samples_per_run`
		plt.plot(data[:,6][run_start:run_end] * sec_per_sample, data[:,plot_type][run_start:run_end], color=colors[(run - 1) % color_cycle_pattern], linewidth=3, label=run_names[run_start] + " | xf " + closest_xfersize(numpy.average(data[:,plot_type][run_start:run_end])) + " | avg: " + `round(numpy.average(data[:,plot_type][run_start:run_end]), 2)`)
		run_start = run * samples_per_run + 1 # skip first data point

	plt.ylabel(plot[plot_type])
	plt.xlabel("seconds")
	plt.legend()

	#major_ticks = numpy.arange(0, 20, 5)
	#minor_ticks = numpy.arange(0, 20, 1)
	ax = plt.axes()
	#ax.set_yticks(major_ticks)
	#ax.set_yticks(minor_ticks, minor=True)
	ax.yaxis.grid(True) # horizontal lines

	fig = plt.gcf()
	fig.set_size_inches(20, 10)
	fig.tight_layout()
	plt.autoscale(tight=True)
	x1,x2,y1,y2 = plt.axis()
	plt.axis((x1,x2,0,y2))
	plt.savefig(os.path.splitext(plot[plot_type] + "." + os.path.basename(flat_file))[0] + "_line.png")
	plt.clf()

	print "Generated " + plot[plot_type] + " line plot - " + os.path.splitext(plot[plot_type] + "." + os.path.basename(flat_file))[0] + "_line.png"

# Read command line args
opts, args = getopt.getopt(sys.argv[1:], 'f:n:haiblcip:')

if not opts:
	print "Missing cmd line options"
	usage()


colors = ['red','orange','teal','green','blue','aqua', 'brown', 'yellow', 'grey']
color_cycle_pattern = 0 # used for color groupings, cycle colors after this number
def get_pattern(xfersizes, samples_per_run):
	for n in range(1, len(xfersizes)/samples_per_run):
		if (closest_xfersize(xfersizes[0]) != closest_xfersize(xfersizes[n*samples_per_run])):
			return n

	return len(colors)

samples_per_run = 0
plot_type = 0 # default plot is all
flat_file = ""
for opt, arg in opts:
    if opt == '-f':
        flat_file = arg
    elif opt == '-n':
        samples_per_run = int(arg)
    elif opt == '-a':
        plot_type = 0
    elif opt == '-i':
        plot_type = 1
    elif opt == '-b':
        plot_type = 2 
    elif opt == '-l':
        plot_type = 3
    elif opt == '-c':
        plot_type = 4
    elif opt == '-p':
        color_cycle_pattern = int(arg)
	if (color_cycle_pattern > len(colors)):
		print "-p " + `color_cycle_pattern` + " (bigger than " + `len(colors)` + ") is not supported\n"
		usage()
    elif opt == '-h':
        usage()

if (flat_file == ""):
    print "-f misssing\n"
    usage()

csv_tmp = "tmp.vdb2gnuplot." + `os.getpid()` + ".flatfile.csv"

if (os.system(vdb_path + " parseflat -i " + flat_file + " -o " + csv_tmp + " -c run -c rate -c MB/sec -c resp -c cpu_used -c xfersize -c interval -c tod 1>/dev/null 2>&1")):
    print "'" + vdb_path + " parseflat -i " + flat_file + "' failed. adjust vdbench path or flatfile path (-f)\n"
    sys.exit(2)

# run,rate,MB/sec,resp,cpu_used,xfersize,interval,tod
tod = numpy.genfromtxt(csv_tmp, skip_header=1, skip_footer=0, delimiter=',', usecols=7, dtype=str)
sec_per_sample = get_sec_per_sample(tod)
run_names = numpy.genfromtxt(csv_tmp, skip_header=1, skip_footer=0, delimiter=',', usecols=0, dtype=str)
data = numpy.genfromtxt(csv_tmp, skip_header=1, skip_footer=0, delimiter=',')
os.system("rm -f " + csv_tmp);

if (samples_per_run == 0):
	samples_per_run = get_samples_per_run(data[:,6])

if (color_cycle_pattern == 0):
	color_cycle_pattern = get_pattern(data[:,5], samples_per_run)

print "Detected " + `sec_per_sample` + " seconds per sample, " + `samples_per_run` + " samples per run, " + `len(data[:,0])/samples_per_run` + " runs, and a pattern of " + `color_cycle_pattern`

if (plot_type == 0):
	generate_bar_plot(1, run_names, data, samples_per_run, color_cycle_pattern, flat_file)
	generate_bar_plot(2, run_names, data, samples_per_run, color_cycle_pattern, flat_file)
	generate_bar_plot(3, run_names, data, samples_per_run, color_cycle_pattern, flat_file)
	generate_bar_plot(4, run_names, data, samples_per_run, color_cycle_pattern, flat_file)
else:
	generate_bar_plot(plot_type, run_names, data, samples_per_run, color_cycle_pattern, flat_file)
	generate_line_plot(plot_type, run_names, data, sec_per_sample, samples_per_run, color_cycle_pattern, flat_file)

sys.exit(0);
